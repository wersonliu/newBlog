# -*-coding:utf8-*-
# !/usr/bin/env python
"""
@Time    : 2018/9/7 9:12
@Author  : wersonliu
"""
from flask import render_template
from app import app, db


@app.errorhandler(404)
def not_found_error(error):
    return render_template('./404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('./500.html'), 500
